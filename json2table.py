import json
import sys
from json2html import jsonconv


j = json.loads(sys.stdin.read())

converter = jsonconv.Json2Html()

print("<html><body>")
print(converter.convert(json=j))
print("</body></html>")
