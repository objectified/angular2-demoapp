import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent implements OnInit {

  constructor(private sanitized: DomSanitizer) { }

  ngOnInit(): void {
  }

  transform(value) {
    console.log(this.sanitized.bypassSecurityTrustHtml(value));
  }

}
